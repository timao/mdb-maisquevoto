webpackJsonp([7],{

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComitesComitesDetalhePageModule", function() { return ComitesComitesDetalhePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__comites_comites_detalhe__ = __webpack_require__(387);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ComitesComitesDetalhePageModule = /** @class */ (function () {
    function ComitesComitesDetalhePageModule() {
    }
    ComitesComitesDetalhePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__comites_comites_detalhe__["a" /* ComitesComitesDetalhePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__comites_comites_detalhe__["a" /* ComitesComitesDetalhePage */]),
            ],
        })
    ], ComitesComitesDetalhePageModule);
    return ComitesComitesDetalhePageModule;
}());

//# sourceMappingURL=comites-comites-detalhe.module.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComitesComitesDetalhePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_comite_comite__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_searchbar_searchbar__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ComitesComitesDetalhePage = /** @class */ (function () {
    function ComitesComitesDetalhePage(navCtrl, navParams, modalCtrl, comiteProvider, utils, auth, sanitizer) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.comiteProvider = comiteProvider;
        this.utils = utils;
        this.auth = auth;
        this.sanitizer = sanitizer;
        this.result = null;
        this.hasLatLng = false;
        this.googleMapsSrc = null;
    }
    ComitesComitesDetalhePage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    ComitesComitesDetalhePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var comiteId = parseInt(this.navParams.get('id'));
        this.comiteProvider.findById(this.auth.getToken(), comiteId).then(function (result) {
            _this.result = result;
            if (result.Longitude && result.Latitude) {
                _this.hasLatLng = true;
                _this.googleMapsSrc = _this.sanitizer.bypassSecurityTrustResourceUrl('http://maps.google.com/maps?q=' + result.Latitude + ',' + result.Longitude + '&z=15&output=embed');
            }
        }, function (err) {
            console.log('error: ', err);
        });
    };
    ComitesComitesDetalhePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-comites-comites-detalhe',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/comites/comites-comites-detalhe/comites-comites-detalhe.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Comites</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n        <ion-icon name="search"></ion-icon>\n      </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content *ngIf="result">\n  <iframe *ngIf="hasLatLng" frameborder="0" style="border:0" allowfullscreen class="map"\n          [src]="googleMapsSrc"></iframe>\n  <div padding>\n    <ion-list class="list">\n      <div ion-item *ngIf="result?.Endereco">\n        <ion-icon name="pin" item-start></ion-icon>\n        {{ result?.Endereco }}\n      </div>\n      <div ion-item *ngIf="result?.HorarioFuncionamento">\n        <ion-icon name="time" item-start></ion-icon>\n        {{ result?.HorarioFuncionamento }}\n      </div>\n      <div ion-item *ngIf="result?.Telefone ">\n        <ion-icon name="phone-portrait" item-start></ion-icon>\n        {{ result?.Telefone }}\n      </div>\n    </ion-list>\n  </div>\n</ion-content>\n<ion-content *ngIf="!result">\n  <p text-center>Carregando...</p>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/comites/comites-comites-detalhe/comites-comites-detalhe.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_comite_comite__["a" /* ComiteProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* DomSanitizer */]])
    ], ComitesComitesDetalhePage);
    return ComitesComitesDetalhePage;
}());

//# sourceMappingURL=comites-comites-detalhe.js.map

/***/ })

});
//# sourceMappingURL=7.js.map