import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComitesPage } from './comites';

@NgModule({
  declarations: [
    ComitesPage,
  ],
  imports: [
    IonicPageModule.forChild(ComitesPage)
  ]
})
export class ComitesPageModule {}
