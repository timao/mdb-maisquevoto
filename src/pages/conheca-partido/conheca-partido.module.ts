import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConhecaPartidoPage } from './conheca-partido';

@NgModule({
  declarations: [
    ConhecaPartidoPage,
  ],
  imports: [
    IonicPageModule.forChild(ConhecaPartidoPage),
  ],
})
export class ConhecaPartidoPageModule {}
