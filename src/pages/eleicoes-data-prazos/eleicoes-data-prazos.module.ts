import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EleicoesDataPrazosPage } from './eleicoes-data-prazos';

@NgModule({
  declarations: [
    EleicoesDataPrazosPage,
  ],
  imports: [
    IonicPageModule.forChild(EleicoesDataPrazosPage),
  ],
})
export class EleicoesDataPrazosPageModule {}
