import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilsProvider } from '../../providers/utils/utils';
import { ConteudoProvider } from '../../providers/conteudo/conteudo';
import { DetailEleicoesPage } from '../detail-eleicoes/detail-eleicoes';

import { SearchbarComponent } from '../../components/searchbar/searchbar';

@IonicPage()
@Component({
  selector: 'page-eleicoes-data-prazos',
  templateUrl: 'eleicoes-data-prazos.html',
})
export class EleicoesDataPrazosPage {

  // Tipo de Conteudo Noticias
  private tipoConteudo = 15;
  private start: number = 100;
  private skip: number = 0;
  anosList: any = [];
  meses:any = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
  DiaMes = [];
  jsonCompleto:any = [];
  searching:boolean = true;
  NadaEncontrado:boolean = false;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public auth: AuthProvider,
    public conteudo: ConteudoProvider,
    public utils: UtilsProvider,
    public modalCtrl: ModalController,
  ) {
    this.loadAnos()
  }

  loadAnos() {
    return new Promise(resolve => {
      this.conteudo.getConteudo(this.auth.getToken(), this.start, this.skip, this.tipoConteudo)
        .then(data => {
          this.anosList = this.anosList.concat(data);
          console.log(data);
          resolve(true);
          this.jsonCompleto = data;
          if(this.anosList.length==0){
            this.NadaEncontrado=true;
          }
          this.pegaDiaMes()
        });

    });
  }

  searchToggle() {
    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();

  }
  openDetails(l){
    this.navCtrl.push(DetailEleicoesPage, {
      interna: l
    })
  }

  pegaDiaMes() {
    for(var j = 0; j < this.jsonCompleto.length; j++) {
        var dataJson = this.jsonCompleto[j].Data.split('/')
        var dia = dataJson[0]
        var mes = dataJson[1];
        this.anosList[j].diaFormatado = dia;
        this.anosList[j].mesFormatado = this.meses[mes - 1].valueOf();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListProgramaPage');
  }
}
