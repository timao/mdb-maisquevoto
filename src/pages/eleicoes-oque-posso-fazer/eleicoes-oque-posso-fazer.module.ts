import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EleicoesOquePossoFazerPage } from './eleicoes-oque-posso-fazer';

@NgModule({
  declarations: [
    EleicoesOquePossoFazerPage,
  ],
  imports: [
    IonicPageModule.forChild(EleicoesOquePossoFazerPage),
  ],
})
export class EleicoesOquePossoFazerPageModule {}
