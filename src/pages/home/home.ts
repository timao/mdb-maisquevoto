import { Component } from '@angular/core';
import { NavController, ActionSheetController, Platform, ModalController, NavParams, LoadingController } from 'ionic-angular';
import { SearchbarComponent } from '../../components/searchbar/searchbar';
import { AgendaProvider } from '../../providers/agenda/agenda';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilsProvider } from '../../providers/utils/utils';
import { ConteudoProvider } from '../../providers/conteudo/conteudo';
import { PersonProvider } from '../../providers/person/person';
import { SocialSharing } from '@ionic-native/social-sharing';
import { SharedService } from '../../providers/shared-service/shared-service';
import { SocialSharePage } from '../social-share/social-share';
import { NotificationProvider } from '../../providers/notificacao/notificacao';
import { Events } from 'ionic-angular';
import { PesquisaPage } from '../pesquisa/pesquisa';
import { QueroContribuirPage } from '../quero-contribuir/quero-contribuir';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [SocialSharing, SharedService]
})
export class HomePage {
  PesquisaPage=PesquisaPage
  QueroContribuirPage=QueroContribuirPage
  tab = "noticias";
  agendaList: any = [];
  noticiasList: any = [];
  private start: number = 3;
  private skip: number = 0;
  profile: any;
  count: 0;
  pages: any;
  FiltroEventos = [
    { name: 'Todos', id: 0, active: true },
    { name: 'Hoje', id: 1, active: false },
    { name: 'Essa Semana', id: 2, active: false },
    { name: 'Esse mês', id: 3, active: false },
  ];

  // Tipo de Conteudo Noticias
  private tipoConteudo = 2;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public actionsheetCtrl: ActionSheetController,
    public platform: Platform,
    public modalCtrl: ModalController,
    public agenda: AgendaProvider,
    public auth: AuthProvider,
    public utils: UtilsProvider,
    public conteudo: ConteudoProvider,
    public person: PersonProvider,
    public notification: NotificationProvider,
    public events: Events,
    public loadingCtrl: LoadingController
  ) {
    this.loadDataUser();
  }


  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.loadNews()
      refresher.complete();
    }, 1000);

  }


  openDetail(page, id) {
    this.navCtrl.push(page, { id: id });
  }

  optionsContribuition() {
    let actionSheet = this.actionsheetCtrl.create({
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Colobare para mudanças',
          icon: 'ios-thumbs-up-outline',
          handler: () => {

          }
        },
        {
          text: 'Responda uma pesquisa',
          icon: 'ios-paper-outline',
          handler: () => {

          }
        },
        {
          text: 'Participe enviando uma sugestão',
          icon: 'ios-text-outline',
          handler: () => {
            console.log('Share clicked');
          }
        },
        {
          text: 'Conheça a proposta de governo',
          icon: 'ios-search-outline',
          handler: () => {
            console.log('Play clicked');
          }
        },
        {
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  searchToggle() {

    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();

  }


  loadNews() {

    return new Promise(resolve => {

      this.conteudo.getConteudo(this.auth.getToken(), this.start, this.skip, this.tipoConteudo)
        .then(data => {

          this.noticiasList = this.noticiasList.concat(data);
          console.log(data);
          resolve(true);

        });

    });

  }

  loadAgenda() {

    return new Promise(resolve => {

      this.agenda.getLista(this.auth.getToken(), this.start, this.skip)
        .then(data => {

          this.agendaList = this.agendaList.concat(data);

          resolve(true);

        });

    });
  }

  doInfinite(infiniteScroll: any, tab) {

    this.start += 3;
    this.skip += 3;

    setTimeout(() => {

      if (this.tab == 'agenda') {
        this.loadAgenda().then(() => {
          infiniteScroll.complete();
        });
      }
      else {
        this.loadNews().then(() => {
          infiniteScroll.complete();
        });
      }

    }, 600);


  }

  estados = new Array();
  cidades: any;

  filter = {
    cidade: '',
    estado: ''
  }

  selectOptEstados = {
    title: 'Selecione o estado',
    subTitle: 'Selecione o estado',
    checked: true
  }

  selectOptCidades = {
    title: 'Selecione a cidade',
    subTitle: 'Selecione a cidade',
    checked: true
  }

  onSelectCidade(uf) {
    this.utils.getCidades(uf).then((result: any) => { this.cidades = result; }, (error) => { });
  }

  getEstados() {
    this.utils.getEstados().then((result: any) => { this.estados = result; }, (error) => { });
  }

  filterByCity(filter) {

    this.start = 3;
    this.skip = 0;

    return new Promise(resolve => {

      this.agenda.filterCity(this.auth.getToken(), filter, this.start, this.skip)
        .then(data => {


          if (data != null) {

            this.agendaList = data;
          }
          else {

            this.utils.showAlert("Nenhum resultado encontrado!");
            this.start = 2;
          }

          resolve(true);
        });
    });
  }

  postLike(conteudo) {

    let idPost = conteudo.Id;

    this.person.get()
      .then((response: any) => {

        let person = (response as any);

        this.conteudo.postLike(idPost, person.Id)
          .then((response: any) => {

            if (response.Curtiu && !conteudo.JaCurtiu) {
              conteudo.Curtidas += 1;
            } else {
              conteudo.Curtidas -= 1;
            }

            conteudo.JaCurtiu = response.Curtiu;

            if (response.GanhouPonto) {
              this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
            }
          });
      });
  }

  ionViewDidLoad() {

    this.getEstados();
    this.loadAgenda();
    this.loadNews();


  }

  toggleClass(element, type: any) {
    let floorElements = document.getElementsByClassName("filtroeventos") as HTMLCollectionOf<HTMLElement>;
    for (var i = 0; i < floorElements.length; i++) {
      if (floorElements[i].innerText != element.currentTarget.innerText) {
        floorElements[i].classList.remove('active');
      }
    }
    for (var j = 0; j < floorElements.length; j++) {
      if (floorElements[j].innerText == element.currentTarget.innerText) {
        floorElements[j].classList.add('active');
      }
    }

    this.filterByData(type);
  }

  filterByData(type: any) {
    return new Promise(resolve => {

      this.agenda.filterData(this.auth.getToken(), type)
        .then(data => {
          this.agendaList = data;
          resolve(true);
        });
    });
  }

  showShare(conteudo) {
    let modal = this.modalCtrl.create(SocialSharePage, conteudo);
    modal.present();
  }

  loadDataUser() {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.getPersonDetail().then((result) => {
      this.getNotificationsCount().then((result2) => {
        loading.dismiss();
        this.events.publish('profile:count', this.profile, this.count);
      });
    });
  }

  getPersonDetail() {
    return new Promise(resolve => {
      this.person.get()
        .then((response: any) => {

          this.profile = response;

          console.log(response);

          resolve(true);
        }, (error) => {
          console.log(error);
        });
    });
  }

  getNotificationsCount() {
    return new Promise(resolve => {
      this.notification.Count().then((response: any) => {

        this.count = response;

        resolve(true);

      }, (error) => {

        this.count = 0;

        console.log(error);
        
        resolve(true);
      });
    });

  }

  navPage(page){
    this.navCtrl.setRoot(page)
  }
}
