import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListCandidatosPage } from './list-candidatos';
import { SearchPipe } from '../../pipes/search/search';

@NgModule({
  declarations: [
    ListCandidatosPage,
    SearchPipe,
  ],
  imports: [
    IonicPageModule.forChild(ListCandidatosPage),
  ],
	exports: [
    SearchPipe
  ]
})
export class ListCandidatosPageModule {}
