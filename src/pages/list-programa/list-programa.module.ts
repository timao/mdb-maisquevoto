import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListProgramaPage } from './list-programa';

@NgModule({
  declarations: [
    ListProgramaPage,
  ],
  imports: [
    IonicPageModule.forChild(ListProgramaPage),
  ],
})
export class ListProgramaPageModule {}
