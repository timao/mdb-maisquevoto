import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController} from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilsProvider } from '../../providers/utils/utils';
import { ConteudoProvider } from '../../providers/conteudo/conteudo';
import { DetailProgramaPage } from '../detail-programa/detail-programa';
import { SearchbarComponent } from '../../components/searchbar/searchbar';
@IonicPage()
@Component({
  selector: 'page-list-programa',
  templateUrl: 'list-programa.html',
})
export class ListProgramaPage {

  // Tipo de Conteudo Noticias
  private tipoConteudo = 14;
  private start: number = 1000;
  private skip: number = 0;
  anosList: any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public auth: AuthProvider,
    public conteudo: ConteudoProvider,
    public modalCtrl: ModalController,
    public utils: UtilsProvider,
    
  ) {
    this.loadAnos()
  }

  loadAnos() {
    return new Promise(resolve => {
      this.conteudo.getConteudo(this.auth.getToken(), this.start, this.skip, this.tipoConteudo)
        .then(data => {
          this.anosList = this.anosList.concat(data);
          console.log(data);
          resolve(true);

        });

    });
  }

  searchToggle() {

    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();

  }


  openDetails(l){
    this.navCtrl.push(DetailProgramaPage, {
      interna: l
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListProgramaPage');
  }

}
