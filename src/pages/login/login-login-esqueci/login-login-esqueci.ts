import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { AuthProvider } from '../../../providers/auth/auth';


@IonicPage({
  name: 'esqueci-senha'
})
@Component({
  selector: 'page-login-login-esqueci',
  templateUrl: 'login-login-esqueci.html',
})
export class LoginLoginEsqueciPage {

  form: any = {
    Email: ''
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private auth: AuthProvider,
    private loadingCtrl: LoadingController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginLoginEsqueciPage');
  }

  EsqueciSubmit() {

    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    let alert = this.alertCtrl.create({
      title: "OK",
      buttons: [
        {
          text: 'Fechar',
          role: 'cancel'
        }
      ]
    })

    loading.present();

    this.auth.postResetPassWord(this.form.Email)

      .then((response) => {

        setTimeout(() => {
          loading.dismiss();
          alert.setTitle(response.toString());
          alert.present();
        }, 2000);

      }, (error) => {
        console.log(error)
        setTimeout(() => {
          if(error.status == 400){
            console.log('aaaaa')
            this.alertCtrl.create({
              title: 'Erro',
              subTitle: error.error.Message,
              buttons: [{ text: 'Ok' }]
            }).present();
          }
        }, 1000);
        loading.dismiss();



      });;
  }

  goLogin() {
    this.navCtrl.setRoot('login');
  }

}
