import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterialInstitucionalPage } from './material-institucional';
import { SearchCategoriasPipe } from '../../pipes/search-categorias/search-categorias';

@NgModule({
  declarations: [
    MaterialInstitucionalPage,
    SearchCategoriasPipe
  ],
  imports: [
    IonicPageModule.forChild(MaterialInstitucionalPage),
  ],
})
export class MaterialInstitucionalPageModule {}
