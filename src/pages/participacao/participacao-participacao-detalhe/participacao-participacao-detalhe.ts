import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ParticipacaoParticipacaoDetalhePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'participacao-detalhe',
  segment: 'participacao/:id'
})
@Component({
  selector: 'page-participacao-participacao-detalhe',
  templateUrl: 'participacao-participacao-detalhe.html',
})
export class ParticipacaoParticipacaoDetalhePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParticipacaoParticipacaoDetalhePage');
  }

}
