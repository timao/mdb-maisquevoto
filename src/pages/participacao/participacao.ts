import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-participacao',
  templateUrl: 'participacao.html',
})
export class ParticipacaoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      
      refresher.complete();
    }, 1000);

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ParticipacaoPage');
  }

  openDetail(page, id) {
    this.navCtrl.push(page, id)
  }

}
