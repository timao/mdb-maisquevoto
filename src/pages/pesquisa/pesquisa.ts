import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { PesquisaProvider } from '../../providers/pesquisa/pesquisa';
import { SearchbarComponent } from '../../components/searchbar/searchbar';

@IonicPage({
  name: 'pesquisa-opniao',
  segment: 'pesquisa'
})
@Component({
  selector: 'page-pesquisa',
  templateUrl: 'pesquisa.html',
})

export class PesquisaPage {

  listaPesquisa: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public loadingCtrl: LoadingController, public pesquisa: PesquisaProvider) {
    this.getData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PesquisaPage');
  }

  searchToggle() {
    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();
  }

  presentProfileModal(pesquisa) {

    if (pesquisa.JaVotou) {
      this.getDetail(pesquisa.Id);
    }
    else {
      let profileModal = this.modalCtrl.create(PesquisaModalPage, pesquisa, {
        showBackdrop: true,
        cssClass: 'modal-box'
      });
      profileModal.present();
    }

  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getData()
      refresher.complete();
    }, 1000);

  }
  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {

      infiniteScroll.complete();
    }, 500);
  }
  NadaEncontrado
  getData() {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.pesquisa.findAll().then((result: any) => {
      loading.dismiss();
      this.listaPesquisa = result;
      console.log(result);
      if(this.listaPesquisa==''){
        this.NadaEncontrado=true; 
      }
    }, (error) => {
      loading.dismiss();
        this.NadaEncontrado=true; 
        console.log(error);
    });


  }

  getDetail(id: any) {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.pesquisa.findById(id).then((result2: any) => {
      loading.dismiss();
      this.navCtrl.push('pesquisa-resposta',  result2 );
      console.log(result2);
    }, (error2) => {
      loading.dismiss();
      console.log(error2);
    });

  }


}


@Component({
  selector: 'modal-pesquisa',
  templateUrl: 'pesquisa-modal.html'
})
export class PesquisaModalPage {
  pesquisaDetail: any;
  constructor(params: NavParams, public navCtrl: NavController, public modalCtrl: ModalController, public loadingCtrl: LoadingController, public pesquisa: PesquisaProvider) {

    this.pesquisaDetail = params.data;

    console.log('objeto: ', params.data);
  }

  close() {
    this.navCtrl.pop();
  }
  getDetail(id: any) {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.pesquisa.findById(id).then((result2: any) => {
      loading.dismiss();
      this.navCtrl.push('pesquisa-resposta',  result2 );
      console.log(result2);
    }, (error2) => {
      loading.dismiss();
      console.log(error2);
    });

  }
  answerQuestion(status) {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    
    loading.present();
    this.pesquisa.post(status).then((result: any) => {
      let profileModal = this.modalCtrl.create(PesquisaModalPage, result, {
        showBackdrop: true,
        cssClass: 'modal-box'
      });
      loading.dismiss();
      profileModal.dismiss();
      this.getDetail(result.Id);
    }, (error) => {
      loading.dismiss();
      console.log(error);
    });
  }


}
