import { Component, OnInit, NgModule } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SearchbarComponent } from "../../components/searchbar/searchbar";
import { BrMaskerModule } from 'brmasker-ionic-3';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms'
import { HomePage } from "../home/home";
import { DoacaoProvider } from '../../providers/doacao/doacao';
import { CONFIG_PROJECT } from '../../providers/app-config';
import { Stripe } from '@ionic-native/stripe';
import { UtilsProvider } from '../../providers/utils/utils';
import { CurrencyPipe } from '@angular/common';

@IonicPage({
  name: 'quero-contribuir',
  segment: 'quero-contribuir'
})
@Component({
  selector: 'page-quero-contribuir',
  templateUrl: 'quero-contribuir.html',
  providers: [Stripe,CurrencyPipe]
})
@NgModule({
  imports: [
    BrMaskerModule
  ]
})
export class QueroContribuirPage implements OnInit {

  candidato:any;
  step: number = 1;

  contribuirForm: FormGroup;
  contribuirStep2Form: FormGroup;
  contribuirStep3Form: FormGroup;
  contribuirStep4Form: FormGroup;
  

  formaPgto: string = 'cartao';

  bandeira = '';

  configuracao: {
    DoacaoValor1: '',
    DoacaoValor2: '',
    DoacaoValor3: '',
    DoacaoValor4: '',
    DoacaoValorMaximo: 0,
    DoacaoValorMinimo: 0
  };

   

  model = {
    ClienteId: 0,
    CPFCNPJ: '',
    Nome: '',
    Email: '',
    Celular: '',
    Sexo: 0,
    Valor2: '',
    CEP: '',
    Complemento: '',
    Endereco: '',
    Cidade: '',
    Estado: '',
    Bairro: '',
    EnderecoNumero: '',
    EventoId: null,
    BemServico: '',
    EspecieRecurso: '',
    StatusSistema: 1,
    NumeroDocumento: '',
    NumeroAutorizacao: '',
    ValidadeDocumento: '',
    NomeDocumento: '',
    DataNascimentoDocumento: '',
    BandeiraDocumento: 0,
    MesDocumento: '',
    AnoDocumento: '',
    check1: false,
    check2: false
  };

  years = new Array();

  erroParte1 = true;
  erroParte2 = true;
  errorNome = false;
  errorEmail = false;
  errorEmail2 = false;
  errorCelular = false;
  errorCPF = false;
  errorCPF2 = false;
  errorSexo = false;
  errorValor = false;
  errorCheck1 = false;
  errorCheck2 = false;

  constructor(private navCtrl: NavController,
    private navParams: NavParams,
    private modalCtrl: ModalController,
    private formBuilder: FormBuilder,
    public BrMaskerModule: BrMaskerModule,
    private doacao: DoacaoProvider,
    private loadingCtrl: LoadingController,
    private stripe: Stripe,
  private utils : UtilsProvider,
  private currencyPipe: CurrencyPipe) {
    this.candidato = this.navParams.get('candidato')
    console.log(this.candidato, 'CAndidato Info')
   this.getDoacaoFromStorage();
  }

  ngOnInit() {
    this.contribuirForm = new FormGroup({
      nome: new FormControl(this.model.Nome, {
        validators: [Validators.required]
      }),
      email: this.formBuilder.control(this.model.Email, [Validators.required, Validators.email]),
      cpf: this.formBuilder.control(this.model.CPFCNPJ, [Validators.required]),
      celular: this.formBuilder.control(this.model.Celular, [Validators.required]),
      sexo: this.formBuilder.control(this.model.Sexo, [Validators.required])
    });

    this.contribuirStep2Form = new FormGroup({
      valor: this.formBuilder.control('', [Validators.required]),
      check1: this.formBuilder.control(false, [Validators.required]),
      check2: this.formBuilder.control(false, [Validators.required]),
    });

    this.contribuirStep3Form = new FormGroup({
      cep: this.formBuilder.control('', [Validators.required]),
      endereco: this.formBuilder.control('', [Validators.required]),
      cidade: this.formBuilder.control('', [Validators.required]),
      estado: this.formBuilder.control('', [Validators.required]),
      bairro: this.formBuilder.control('', [Validators.required]),
      complemento: this.formBuilder.control(''),
      numero: this.formBuilder.control(''),
    });

    this.contribuirStep4Form = new FormGroup({
      recurso: this.formBuilder.control('', [Validators.required]),
      documento: this.formBuilder.control('', [Validators.required]),
      cvv: this.formBuilder.control('', [Validators.required]),
      mes: this.formBuilder.control('', [Validators.required]),
      ano: this.formBuilder.control('', [Validators.required]),
      nomeimpresso: this.formBuilder.control('', [Validators.required]),
      data: this.formBuilder.control('', [Validators.required]),
    });
  }

  searchToggle() {
    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();
  }

  ionViewDidLoad() {

    this.carregarConfig().then(() => {
      let step = parseInt(this.navParams.get('step'));
      if (step) {
        this.step = step;
      } else {
        this.getDoacaoFromStorage();
        this.step = 1;
      }
    });
  }

  goBack() {
    this.navCtrl.pop().catch(() => {
      this.navCtrl.setRoot('HomePage');
    });
  }

  goToStep(step: number, model) {

    if (model != undefined) 
    {
      localStorage.setItem('doacao', JSON.stringify(model));
    }

    this.validaCampos1(model);
    this.navCtrl.push(QueroContribuirPage, { step: step });
  }

  carregarConfig() {
    return new Promise(resolve => {
      this.doacao.get().then((result: any) => {
        this.configuracao = result;
        console.log(result);
        resolve(true);
      });
    });
  }

  enviarDoacao(model) {

    let vish = '';


    this.stripe.getCardType(model.NumeroDocumento)
      .then(response => {

        this.bandeira = response;

      });
  }

  loadYears() {
    var y = (new Date()).getFullYear();
    for (var i = 0; i <= 12; i++) {
      this.years.push(y + i);
    }
  }

  validaPart1() {

    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    let validaemail = re.test(this.model.Email);
    
    var validacpf = this.utils.cpf(this.model.CPFCNPJ.replace(".","").replace(".","").replace("-",""));

    if (this.model.Nome != '' && (this.model.Email!= '' && validaemail) && this.model.Celular != '' && (this.model.CPFCNPJ != '' && validacpf) && this.model.Sexo > 0) {
     
      this.erroParte1 = false;

      return true;
    }
    else {
      return false;
    }
  }

  validaPart2() {

    if (this.model.Valor2 != '' && 
    parseFloat(this.model.Valor2.replace("R$ ","")) >= this.configuracao.DoacaoValorMinimo &&
    parseFloat(this.model.Valor2.replace("R$ ","")) <= this.configuracao.DoacaoValorMaximo && 
    this.model.check1 && 
    this.model.check2) {
     
      this.erroParte2 = false;

      return true;
    }
    else {
      return false;
    }
  }

  validaCampos1(model)
  {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(model.Nome == '')
    {
      this.errorNome = true;
      this.erroParte1 = true;
    }
    else if(model.Email == '')
    {
      this.errorEmail = true;
      this.erroParte1 = true;
    }
    else if(!re.test(model.Email))
    {
      this.errorEmail2 = true;
      this.erroParte1 = true;
    }
    else if(model.Celular == '')
    {
      this.errorCelular = true;
      this.erroParte1 = true;
    }
    else  if(model.CPFCNPJ == '')
    {
      this.errorCPF = true;
      this.erroParte1 = true;
    }
    else if(!this.utils.cpf(model.CPFCNPJ.replace(".","").replace(".","").replace("-","")))
    {
      this.errorCPF2 = true;
      this.erroParte1 = true;
    }
    else{
      this.erroParte1 = false;
    }

    console.log(this.erroParte1);
  }

  getDoacaoFromStorage()
  {
    if (localStorage.getItem('doacao') != null) {
      this.model = JSON.parse(localStorage.getItem('doacao'));
      console.log(this.model);
      this.validaCampos1(this.model);
    }
  }

  selecionarValor(valor)
  {
    this.model.Valor2 = valor;
  }

  convert(amount: any)
  {
    var money  = amount.target.value.replace(",",".");
    this.model.Valor2 = this.currencyPipe.transform(money, 'R$', true, '1.2-2');
    this.validaPart2();
  }
}
