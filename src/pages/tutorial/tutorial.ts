import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AuthProvider } from '../../providers/auth/auth';

@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {

  hiddenNext:boolean = false

  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthProvider, public loading:LoadingController) {}

  ionViewDidLoad() {}

  goState() {
    let loading = this.loading.create({
      content: 'Carregando...'
    });
    let TIME_IN_MS = 3000;

    loading.present()
    this.auth.postLogin()
    let hideFooterTimeout = setTimeout( () => {
      loading.dismiss()
      this.navCtrl.setRoot(HomePage) 
    }, TIME_IN_MS);
  
  }


  goToSlide() {

    this.slides.slideNext()
  }

  slideChanged() {

    this.slides.isEnd() == true ? this.hiddenNext = true : this.hiddenNext = false

  }
  
}
