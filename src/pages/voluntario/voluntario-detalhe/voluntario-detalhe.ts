import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { IndiqueAmigosPage } from '../indique-amigos';
import { SearchbarComponent } from '../../../components/searchbar/searchbar';
import { VoluntarioProvider } from '../../../providers/voluntario/voluntario';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { PersonProvider } from '../../../providers/person/person';
import { UtilsProvider } from '../../../providers/utils/utils';
import { SocialSharePage } from '../../social-share/social-share';
import { ConteudoProvider } from '../../../providers/conteudo/conteudo';

@IonicPage({
  name: 'voluntario-detalhe',
  segment: 'voluntario-detalhe/:id'
})
@Component({
  selector: 'page-voluntario-detalhe',
  templateUrl: 'voluntario-detalhe.html',
})
export class VoluntarioDetalhePage {

  vol: any;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public modalCtrl: ModalController,
     public voluntario: VoluntarioProvider, 
     public loadingCtrl: LoadingController,
     public utils: UtilsProvider,
     public person: PersonProvider,
     private _conteudo: ConteudoProvider,
    ) {
    this.getData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VoluntarioDetalhePage');
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getData()
      refresher.complete();
    }, 1000);

  }

  openModal() {
    let modal = this.modalCtrl.create(IndiqueAmigosPage);
    modal.present();
  }

  searchToggle() {

    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();
    
  }
 
  getData() {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.voluntario.findById(this.navParams.data).then((response: Object) => {
      loading.dismiss();
      this.vol = response;
      console.log(response);
    }, (error) => {
      loading.dismiss();
      console.log(error);
    });
  }

  postLike(conteudo) {

    let idPost = conteudo.Id;

    this.person.get()
      .then((response: any) => {

        let person = (response as any);

        this._conteudo.postLike(idPost, person.Id)
          .then((response: any) => {

            if (response.Curtiu && !conteudo.JaCurtiu) {
              conteudo.Curtidas += 1;
            } else {
              conteudo.Curtidas -= 1;
            }

            conteudo.JaCurtiu = response.Curtiu;

            if (response.GanhouPonto) {
              this.utils.showModalSucesso(response.Mensagem,"Continue interagindo" ,"OK");
            }
          });
      });
  }

  postShare(conteudo) {
    let modal = this.modalCtrl.create(SocialSharePage, conteudo);
    modal.present();
  }
}
