import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController, Platform, ModalController } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { File } from '@ionic-native/file';

declare var cordova: any;
@Injectable()
export class DownloadProvider {
  storageDirectory:string = ''

  constructor(
    public http: HttpClient,
    private transfer: FileTransfer,
    private alertCtrl: AlertController,
    public platform: Platform,
    private androidPermissions: AndroidPermissions,
    public modalCtrl: ModalController,
    private file: File
  ) {
    console.log('Hello DownloadProvider Provider');
    this.platform.ready().then(() => {
      // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
      if(!this.platform.is('cordova')) {
        return false;
      }

      if (this.platform.is('ios')) {
        this.storageDirectory = cordova.file.documentsDirectory;
      }
      else if(this.platform.is('android')) {
        this.storageDirectory = cordova.file.dataDirectory;
      }
      else {
        // exit otherwise, but you could add further types here e.g. Windows
        return false;
      }
    });
    }

  showAlert(text, message = undefined) {
    let alert = this.alertCtrl.create({
      title: "OK",
      buttons: [
        {
          text: 'Fechar',
          role: 'cancel'
        }
      ]
    })
  }
  download(url: any) {
    if (!this.platform.is('cordova')) {
      return false;
    }

    let fileTransfer: FileTransferObject = this.transfer.create();
    let fileName = url.substring(url.lastIndexOf('/') + 1);

    let directory = '';

    if (this.platform.is('ios')) {
      directory = cordova.file.documentsDirectory;
      fileTransfer.download(url, directory + fileName).then((entry) => {
        this.showAlert('Download concluído');
        console.log('download complete: ' + entry.toURL());
      }, (error) => {
        console.log(error);
        // handle error
      });
    }
    else if (this.platform.is('android')) {
      directory = 'file:///storage/emulated/0/Download/';
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
      .then(result => {
        console.log(result.hasPermission)
        if (!result.hasPermission) {
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
        }
        else {
          fileTransfer.download(url, directory + fileName).then((entry) => {
            this.showAlert('Download concluído');
            console.log('download complete: ' + entry.toURL());
          }, (error) => {
            console.log(error);
            // handle error
          });
        }
      },
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
      );
    }

    
    }
  
  }