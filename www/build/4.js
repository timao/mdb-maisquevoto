webpackJsonp([4],{

/***/ 380:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificacoesNotificacoesDetalhePageModule", function() { return NotificacoesNotificacoesDetalhePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notificacoes_notificacoes_detalhe__ = __webpack_require__(391);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NotificacoesNotificacoesDetalhePageModule = /** @class */ (function () {
    function NotificacoesNotificacoesDetalhePageModule() {
    }
    NotificacoesNotificacoesDetalhePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__notificacoes_notificacoes_detalhe__["a" /* NotificacoesNotificacoesDetalhePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__notificacoes_notificacoes_detalhe__["a" /* NotificacoesNotificacoesDetalhePage */]),
            ],
        })
    ], NotificacoesNotificacoesDetalhePageModule);
    return NotificacoesNotificacoesDetalhePageModule;
}());

//# sourceMappingURL=notificacoes-notificacoes-detalhe.module.js.map

/***/ }),

/***/ 391:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificacoesNotificacoesDetalhePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_notificacao_notificacao__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NotificacoesNotificacoesDetalhePage = /** @class */ (function () {
    function NotificacoesNotificacoesDetalhePage(navCtrl, navParams, notification, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.notification = notification;
        this.loadingCtrl = loadingCtrl;
        this.getNotification();
    }
    NotificacoesNotificacoesDetalhePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NotificacoesNotificacoesDetalhePage');
    };
    NotificacoesNotificacoesDetalhePage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getNotification();
            refresher.complete();
        }, 1000);
    };
    NotificacoesNotificacoesDetalhePage.prototype.getNotification = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        console.log(this.navParams.data);
        this.notification.GetDetail(this.navParams.data).then(function (response) {
            loading.dismiss();
            _this.notif = response;
            console.log(response);
        }, function (error) {
            loading.dismiss();
            console.log(error);
        });
    };
    NotificacoesNotificacoesDetalhePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-notificacoes-notificacoes-detalhe',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/notificacoes/notificacoes-notificacoes-detalhe/notificacoes-notificacoes-detalhe.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Notificações</ion-title>\n    <ion-buttons end>\n      <!-- <button  class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n    <ion-card>\n      <img src="assets/imgs/fakes/bolso.jpg">\n      <ion-card-content>\n        <div>\n          <ion-card-title *ngIf="notif">{{notif.Titulo}}</ion-card-title>\n          <ion-note *ngIf="notif">  <ion-icon name="calendar"></ion-icon> {{notif.DataCriacao}}</ion-note>\n        </div>\n        <div class="padding-border-top">\n            <p *ngIf="notif" [innerHTML]="notif.Mensagem"></p>\n        </div>\n      </ion-card-content>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/notificacoes/notificacoes-notificacoes-detalhe/notificacoes-notificacoes-detalhe.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_notificacao_notificacao__["a" /* NotificationProvider */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */]])
    ], NotificacoesNotificacoesDetalhePage);
    return NotificacoesNotificacoesDetalhePage;
}());

//# sourceMappingURL=notificacoes-notificacoes-detalhe.js.map

/***/ })

});
//# sourceMappingURL=4.js.map